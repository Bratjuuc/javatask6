package collections;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

public class CollectionsDemoTest {

    @DataProvider
    public Object[][] strings() {

        return new Object[][]{
                {"Кверти", "Йцукенович", "Асдф",
                 "Питон", "Керасович", "Тензорфлоу",
                 "Cogito", "Ergo", "Sum",
                 "Cute", "Little", "Pony",
                 "Sample", "Plain", "Text",
                 "Super", "Meat", "Boy",
                 "Functor", "Applicative", "Monad",
                 "Hyper", "Light", "Drifter"}
        };
    }

    @Test(dataProvider = "strings")
    public void testNumberOne(String[] testSubjects) {
        Integer expected = 2, result = CollectionsDemo.numberOne(Arrays.asList(testSubjects), 'C');
        Assert.assertEquals(result, expected);
    }
}