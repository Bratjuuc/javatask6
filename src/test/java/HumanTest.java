import org.testng.Assert;
import org.testng.annotations.DataProvider;
import сlasses.Human;

public class HumanTest {

    @DataProvider
    public Object[][] humans() {

        return new Object[][]{
                {12, "Кверти", "Йцукенович", "Асдф"},
                {34, "Питон", "Керасович", "Тензорфлоу"},
                {66, "Cogito", "Ergo", "Sum"},
                {27, "Cute", "Little", "Pony"},
                {27, "Sample", "Plain", "Text"},
                {25, "Super", "Meat", "Boy"},
                {33, "Functor", "Applicative", "Monad"},
                {21, "Hyper", "Light", "Drifter"}
        };
    }

    @org.testng.annotations.Test(dataProvider = "humans")
    public void testTestEquals(int age, String fName, String mName, String lName) {
        Human human81 = new Human(1001, "al-Masih", "ad", "Dajjal");
        Human test = new Human(age, fName, mName, lName);

        Assert.assertNotEquals(human81, test);
        Assert.assertEquals(test, test);
    }

    @org.testng.annotations.Test
    public void testCompareTo() {
        //lName > fName > mName > age
        Human[] array = {
                new Human(1, "aaa", "aaa", "aaa"),
                new Human(1, "aaa", "aaa", "bbb"),
                new Human(1, "ccc", "aaa", "bbb"),
                new Human(1, "ccc", "ddd", "bbb"),
                new Human(2, "ccc", "ddd", "bbb"),
                new Human(1, "aaa", "aaa", "ccc")};

        for (int i = 0; i < array.length - 1; i++) {
            Assert.assertTrue(array[i].compareTo(array[i + 1]) < 0);
        }
    }

    @org.testng.annotations.Test
    public void testTestHashCode() {
    }

    @org.testng.annotations.Test(dataProvider = "humans")
    public void testSetFirstName(int age, String fName, String mName, String lName) {
        Human test = new Human(age, fName, mName, lName);
        String newName = "Abba";
        test.setFirstName(newName);
        Assert.assertEquals(test.getFirstName().compareTo( newName),0);
    }

    @org.testng.annotations.Test(dataProvider = "humans")
    public void testSetMiddleName(int age, String fName, String mName, String lName) {
        Human example = new Human(age, fName, mName, lName);

        String expected = "Test";
        example.setMiddleName("Test");
        Assert.assertEquals(example.getMiddleName().compareTo(expected), 0);

    }

    @org.testng.annotations.Test(dataProvider = "humans")
    public void testSetLastName(int age, String fName, String mName, String lName) {
        Human example = new Human(age, fName, mName, lName);

        String expected ="Test";
        example.setLastName("Test");
        Assert.assertEquals(example.getLastName().compareTo(expected), 0);
    }

    /*@org.testng.annotations.Test(dataProvider = "humans")
    public void testSetName(int age, String fName, String mName, String lName) {
        Human example = new Human(age, fName, mName, lName),
                dummy = new Human(age, "Test0", "Test1", "Test2");
        example.setName()
        Assert.assertEquals(example, dummy);
    }*/
}