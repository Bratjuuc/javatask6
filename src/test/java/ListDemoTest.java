import org.testng.Assert;
import org.testng.annotations.Test;
import сlasses.Human;
import сlasses.ListDemo;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ListDemoTest {

    Human[] temp = {new Human(12, "Кверти", "Йцукенович", "Асдф"),
            new Human(34, "Питон", "Керасович", "Тензорфлоу"),
            new Human(66, "Cogito", "Ergo", "Sum"),
            new Human(27, "Cute", "Little", "Pony"),
            new Human(27, "Sample", "Plain", "Text"),
            new Human(25, "Super", "Meat", "Boy"),
            new Human(25, "Hyper", "Meat", "Boy"),
            new Human(25, "Hyper", "Duper", "Boy"),
            new Human(33, "Functor", "Applicative", "Monad"),
            new Human(21, "Hyper", "Light", "Drifter")};

    public List<Human> humans = new LinkedList<>(Arrays.asList(temp));
    public Human sample = new Human(33, "Very", "Unique", "Human");

    @Test
    public void testHasTheSameLastName() {
        List<Human> test = ListDemo.hasTheSameLastName(humans, temp[5]);
        Assert.assertFalse(test.isEmpty());

        for (Human h : test) {
            Assert.assertTrue(ListDemo.hasSameLastName.apply(temp[5]).apply(h));
        }
        Assert.assertFalse(ListDemo.hasSameLastName.apply(temp[5]).apply(temp[4]));
    }

    @Test
    public void testIsntTheSamePerson() {
        List<Human> test = ListDemo.isntTheSamePersonFilter(humans, sample);
        Assert.assertFalse(test.isEmpty());
        for (Human h : humans) {
            Assert.assertTrue(ListDemo.isntSamePerson.apply(sample).apply(h));
        }
        Assert.assertFalse(ListDemo.isntSamePerson.apply(sample).apply(sample));
    }

    @Test
    public void testDoesntIntersectFilter() {

    }

    @Test
    public void testGetMaxAge() {
    }

    @Test
    public void testSetOfHumansWithMaxAge() {
    }

    @Test
    public void testMatchID() {
    }

    @Test
    public void testIsMature() {
    }

    @Test
    public void testGetAge() {
    }

    @Test
    public void testAgeMap() {
    }

    @Test
    public void testElewen() {
    }
}