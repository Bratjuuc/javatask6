import collections.CollectionsDemo;
import org.junit.Assert;
import org.junit.Test;
import сlasses.FuncProg;
import сlasses.Human;
import сlasses.ListDemo;
import сlasses.Student;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static сlasses.ListDemo.*;

public class CollectionsTest {

    @Test
    public void qtStrFirstCharTest() {
        List<String> list = new ArrayList<>(Arrays.asList(
                "Мама мыла раму",
                "Просто тестовая строка",
                "Миксер",
                "маленький миксер",
                "English stroka"));
        Assert.assertEquals(2, CollectionsDemo.numberOne(list, 'М'));

        list.clear();
        assertEquals(0, CollectionsDemo.numberOne(list, 'м'));
    }

    @Test
    public void getNamesakesTest() {
        List<Human> list = new ArrayList<>(Arrays.asList(
                new Human(0,"Имя", "Отчество", "Фамилия"),
                new Human(0,"Такой-то", "Николаевич", "Такойтов"),
                new Human(0,"какой то", "Отчество", "Такойтов"),
                new Human(0,"   ", " ", " "),
                new Human(0,"Обычный", "Человек", "Такойтов")));

        assertEquals(new ArrayList<Human>(Arrays.asList(
                new Human(0,"Такой-то", "Николаевич", "Такойтов"),
                new Human(0,"какой то", "Отчество", "Такойтов"),
                new Human(0,"Обычный", "Человек", "Такойтов"))),
                hasTheSameLastName(list, new Human(0," ",  " ","Такойтов")));
    }

/*    @Test
    public void noHumanSelectTest() {
        List<Human> list = new ArrayList<>(Arrays.asList(
                new Human("Имя", "Фамилия", "Отчество", new GregorianCalendar(0, Calendar.FEBRUARY, 0)),
                new Human("Такой-то", "Такойтов", "Николаевич", new GregorianCalendar(1975, Calendar.JUNE, 22)),
                new Human("Имя", "Фамилия", "Отчество", new GregorianCalendar(0, Calendar.FEBRUARY, 0)),
                new Human("какой то", "такойтов", "Отчество", new GregorianCalendar(0, Calendar.JANUARY, 0)),
                new Human("   ", " ", "", new GregorianCalendar(666, Calendar.AUGUST, 17, 11, 45)),
                new Human("Обычный", "Такойтов", "Человек", new GregorianCalendar(5, Calendar.APRIL, 5))));

        assertEquals(new ArrayList<>(Arrays.asList(
                new Human("Такой-то", "Такойтов", "Николаевич", new GregorianCalendar(1975, Calendar.JUNE, 22)),
                new Human("какой то", "такойтов", "Отчество", new GregorianCalendar(0, Calendar.JANUARY, 0)),
                new Human("   ", " ", "", new GregorianCalendar(666, Calendar.AUGUST, 17, 11, 45)),
                new Human("Обычный", "Такойтов", "Человек", new GregorianCalendar(5, Calendar.APRIL, 5)))),
                noHumanSelect(list, new Human("Имя", "Фамилия", "Отчество", new GregorianCalendar(0, Calendar.FEBRUARY, 0))));
    }*/

    @Test
    public void notIntersectTest() {
        List<Set<Integer>> list = new ArrayList<>(Arrays.asList(
                new HashSet<>(Arrays.asList(0, 1, 2, 3, 4, 5)),
                new HashSet<>(Arrays.asList(3, 4, 5, 6)),
                new HashSet<>(Arrays.asList(7, 8, 9)),
                new HashSet<>(Arrays.asList(10, 0, -5, -2, 0))));

        assertEquals(Collections.singletonList(new HashSet<>(Arrays.asList(7, 8, 9))),
                ListDemo.doesntIntersectFilter(list, new HashSet<>(Arrays.asList(0, 3, 4))));
    }

    @Test
    public void maxAgeTest() {
        List<Human> list = new ArrayList<>(Arrays.asList(
                new Human(7, "Имя", "Фамилия", "Отчество"),
                new Human(3,"Такой-то", "Такойтов", "asdas"),
                new Student(10,"Студент", "Обыкновенный", "ИзИМИТ", "Филологический"),
                new Human(1,"/", "/", "/"),
                new Human(10),
                new Student(9,"Обычный", "Такойтов", "Человек", "ИМИТ")));

        int maxAge = ListDemo.getMaxAge(list);
        assertEquals(new LinkedHashSet<>(Arrays.asList(
                new Human(10),
                new Student(10,"Студент", "Обыкновенный", "ИзИМИТ", "Филологический"))),
                list.stream().filter(s ->s.getAge() == maxAge).collect(Collectors.toSet()));
                //FuncProg.filter(list, s -> s.getAge() == maxAge, new LinkedHashSet<>()));
    }

    /*@Test
    public void getSortListTest() {
        Set<Human> set = new HashSet<>(Arrays.asList(
                new Human(0,"Имя", "Фамилия", "Отчество"),
                new Human(0, "Такой-то", "Такойтов", "Николаевич"),
                new Student(0, "Студент", "Обыкновенный", "ИзИМИТ", "Филологический"),
                new Human(0, "Имя", "НеФамилия", "Отчество"),
                new Human(0, "   ", " ", ""),
                new Student(0, "Обычный", "Такойтов", "Человек", "ИМИТ")));

        assertEquals(new ArrayList<>(Arrays.asList(
                new Human("   ", " ", ""),
                new Human("Имя", "НеФамилия", "Отчество"),
                new Student("Студент", "Обыкновенный", "ИзИМИТ" , "Филологический"),
                new Student("Обычный", "Такойтов", "Человек", "ИМИТ"),
                new Human("Такой-то", "Такойтов", "Николаевич"),
                new Human("Имя", "Фамилия", "Отчество"))),

                getSortList(set));
    }*/

    @Test
    public void perIndContainsTest() {
        Map<Integer, Human> map = new HashMap<>();
        map.put(-2, new Human("Имя", "Фамилия", "Отчество"));
        map.put(0, new Human("Имя", "Фамилия", "Отчество"));
        map.put(1, new Human("Такой-то", "Такойтов", "Николаевич"));
        map.put(199, new Human());

        assertEquals(new HashSet<>(Arrays.asList(
                new Human("Имя", "Фамилия", "Отчество"),
                new Human("Такой-то", "Такойтов", "Николаевич"))),
                getHumansWithIDs(map, new HashSet<>(Arrays.asList(-2, -1, 1, 1, 2))));
    }

    @Test
    public void notLess18YearsTest() {
        Map<Integer, Human> map = new HashMap<>();
        map.put(1, new Human(20,"Имя", "Фамилия", "Отчество"));
        map.put(2, new Human(10,"Имя", "Фамилия", "Отчество"));
        map.put(3, new Human(30,"Такой-то", "Такойтов", "Николаевич"));
        map.put(4, new Human(1));

        Set<Integer> expected = new TreeSet<Integer>();
        expected.add(1);
        expected.add(3);
        assertEquals(expected,
                isMature(map));
    }

    @Test
    public void idOnAgePersonTest() {
        Map<Integer, Human> map = new HashMap<>();
        map.put(1, new Human(18,"Имя", "Фамилия", "Отчество"));
        map.put(2, new Human(15,"Имя", "Фамилия", "Отчество"));
        map.put(3, new Human(243,"Такой-то", "Такойтов", "Николаевич"));
        map.put(4, new Human(1313));

        Map<Integer, Integer> mapOtv = new HashMap<>();
        mapOtv.put(1, 18);
        mapOtv.put(2, 15);
        mapOtv.put(3, 243);
        mapOtv.put(4, 1313);

        assertEquals(mapOtv, getAgeMap(map));
    }

    @Test
    public void ageOnListPersonTest() {
        Set<Human> set = new LinkedHashSet<>(Arrays.asList(
                new Student(46, "Студент", "Обыкновенный", "ИзИМИТ", "Филологический"),
                new Human(46,"   ", " ", " "),
                new Student(20,"Обычный", "Такойтов", "Человек", "ИМИТ"),
                new Human(46,"Имя", "Фамилия", "Отчество")));

        Map<Integer, List<Human>> mapOtv = new HashMap<>();
        mapOtv.put(46, new ArrayList<>(Arrays.asList(
                new Human(46,"Студент", "Обыкновенный", "ИзИМИТ"),
                new Human(46, "   ", " ", " "),
                new Human(46, "Имя", "Фамилия", "Отчество"))));
        mapOtv.put(20, new ArrayList<>(Collections.singletonList(new Human(20,"Обычный", "Такойтов", "Человек"))));

        assertEquals(mapOtv, ageMap(set));
    }

    @Test
    public void ageSymbolOnListPersonTest() {
        Set<Human> set = new LinkedHashSet<>(Arrays.asList(
                new Student(46, "Студент", "Обыкновенный", "ИзИМИТ", "Филологический"),
                new Human(46, "   ", "Обычный", "Sample"),
                new Student(20,"Такойтов", "Обычный", "Человек", "ИМИТ"),
                new Human(46,"Имя", "Фамилия", "Отчество")));

        Map<Character, List<Human>> m1 = new HashMap<>();
        m1.put('О', new ArrayList<>(Arrays.asList(
                new Human(46, "Студент", "Обыкновенный", "ИзИМИТ"),
                new Human(46,"   ", "Обычный", "Sample"))));
        m1.put('Ф', new ArrayList<>(Collections.singletonList(new Human(46, "Имя", "Фамилия", "Отчество"))));
        Map<Character, List<Human>> m2 = new HashMap<>();
        m2.put('О', new ArrayList<>(Collections.singletonList(new Human(20, "Такойтов", "Обычный", "Человек"))));

        Map<Integer, Map<Character, List<Human>>> mapOtv = new HashMap<>();
        mapOtv.put(46, m1);
        mapOtv.put(20, m2);

        assertEquals(mapOtv, taskElewen(set));
    }
}