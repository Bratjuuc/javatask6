package collections;

import org.jetbrains.annotations.NotNull;

import java.util.*;

public class CollectionsDemo {
    public static int numberOne(@NotNull List<String> strings, char ch) {
        int result = 0;
        for (String i : strings) {
            if (i.charAt(0) == ch) {
                result++;
            }
        }
        return result;
    }
}