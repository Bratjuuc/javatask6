package collections;

import сlasses.FuncProg;
import сlasses.Human;

import java.util.*;

public class PhoneBook {
    public Map<Human, List<StringBuilder>> phoneNumbers;

    public PhoneBook() {
        phoneNumbers = new HashMap<>();
    }

    public void addNumber(Human person, StringBuilder number) {
        if (!phoneNumbers.containsKey(person)) {
            List<StringBuilder> temp = new LinkedList<>();
            temp.add(number);
            phoneNumbers.put(person, temp);
        } else if (!phoneNumbers.get(person).contains(number)) {
            phoneNumbers.get(person).add(number);
        }
    }

    public void deleteNumber(Human person, StringBuilder number) {
        if (phoneNumbers.containsKey(person)) {
            phoneNumbers.get(person).remove(number);
        }
    }

    public Human findPersonByNumber(StringBuilder number) {
        for (Human i : phoneNumbers.keySet()) {
            if (phoneNumbers.get(i).contains(number)) {
                return  i;
            }
        }
        return null;
    }

    public Map<Human, List<StringBuilder>> filterByName(String prefix) {
        List<Human> newList = (List<Human>) FuncProg.filter(phoneNumbers.keySet(),
                person -> person.getLastName().toString().startsWith(prefix),
                new LinkedList<Human>());

        Map<Human, List<StringBuilder>> result = new HashMap<>();
        for (Human i : newList) {
            result.put(i, phoneNumbers.get(i));
        }
        return result;
    }
}