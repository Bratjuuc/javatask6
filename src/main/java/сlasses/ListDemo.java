package сlasses;

import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ListDemo {

    public static List<Human> hasTheSameLastName(List<Human> list, Human sample) {
        return FuncProg.filter(list, hasSameLastName.apply(sample), new ArrayList<Human>(list.size()));
    }

    public static List<Human> isntTheSamePersonFilter(List<Human> list, Human sample) {
        return FuncProg.filter(list, isntSamePerson.apply(sample), new ArrayList<Human>(list.size()));
    }

    public static List<Set<Integer>> doesntIntersectFilter(List<Set<Integer>> list, Set<Integer> sample) {
        /*List<Set<Integer>> result = new ArrayList<>(list.size());
        for (Set<Integer> i : list){
            if (doesntIntersect(sample, i)){
                result.add(new TreeSet<>(i));
            }
        }
        return result;*/
        return FuncProg.filter(list, doesntIntersect.apply(sample), new ArrayList<>());
    }

    public static <T extends Human, M extends Collection<T>> int getMaxAge(@NotNull M list) {
        int max = 0;
        for (Human i : list) {
            if (max < i.getAge()) {
                max = i.getAge();
            }
        }
        return max;
    }

    public static Set<Human> setOfHumansWithMaxAge(Set<Human> list) {
        return FuncProg.filter(list, isOfAgeOf.apply(getMaxAge(list)), new HashSet<Human>(list.size()));
    }

    public static @NotNull Set<Human> matchID(@NotNull Map<Integer, Human> map, @NotNull Set<Integer> set) {
        Set<Human> result = new HashSet<>(map.size());
        for (Integer i : set) {
            if (map.containsKey(i)) {
                result.add(map.get(i).iClone());
            }
        }
        return result;
    }

    public static @NotNull Set<Integer> isMature(@NotNull Map<Integer, Human> map) {
        Set<Integer> result = new HashSet<>(map.size());

        for (Map.Entry<Integer, Human> i : map.entrySet()) {
            if (i.getValue().getAge() >= 18) {
                result.add(i.getKey());
            }
        }
        return result;
    }

    public static @NotNull Map<Integer, Integer> getAgeMap(@NotNull Map<Integer, Human> map) {
        Map<Integer, Integer> result = new HashMap<>(map.size());

        for (Integer i : map.keySet()) {
            result.put(i, map.get(i).getAge());
        }
        return result;
    }

    public static @NotNull Map<Integer, List<Human>> ageMap(@NotNull Set<Human> set) {
        Map<Integer, List<Human>> result = new HashMap(set.size());

        for (Human i : set) {
            if (!result.containsKey(i.getAge())) {
                result.put(i.getAge(), new LinkedList<>());
            }
            result.get(i.getAge()).add(i);
        }
        return result;
    }

    public static Function<Human, Function<Human, Boolean>>
            hasSameLastName = person1 -> person2 -> person1.getLastName().equals(person2.getLastName()),
            isntSamePerson = person1 -> person2 -> !person1.equals(person2);

    public static Function<Integer, Function<Human, Boolean>> isOfAgeOf = age -> person -> person.getAge() == age;

    public static Function<Set<Integer>, Function<Set<Integer>, Boolean>> doesntIntersect = set1 -> set2 ->//public static Boolean doesntIntersect(Set<Integer> set1, Set<Integer> set2)
    {
        for (Integer i : set1) {
            if (set2.contains(i)) {
                return false;
            }
        }
        return true;
    };

    public static @NotNull Set<Human> getHumansWithIDs(Map<Integer, Human> map, Set<Integer> set){
        Set<Human> result = new HashSet<>();

        for (Integer i : set){
            if (map.get(i) != null)
                result.add(map.get(i));
        }

        return result;
        //return set.stream().map(map::get).collect(Collectors.toCollection(HashSet::new));
    }

    public static @NotNull List<Integer> getMatureHumansIDs(Map<Integer, Human> map){
        List<Integer> result = new LinkedList<>();

        for (Integer i : map.keySet()){
            if (map.get(i).getAge() >= 18){
                result.add(i);
            }
        }

        return result;
        //return map.keySet().stream().filter(s -> map.get(s).getAge() >= 18).collect(Collectors.toList());
    }

    public static @NotNull Map<Integer, Map<Character, List<Human>>> taskElewen(@NotNull Set<Human> humans) {
        Map<Integer, Map<Character, List<Human>>> result = new HashMap<>();

        for (Human h : humans) {
            Character fChar = h.getMiddleName().charAt(0);
            Integer age = h.getAge();

            if (!result.containsKey(age)) {
                Map<Character, List<Human>> tempMap = new HashMap<>();
                List<Human> temp = new ArrayList<>();
                temp.add(h);
                tempMap.put(fChar, temp);
                result.put(age, tempMap);
            } else if (!result.get(age).containsKey(fChar)) {
                List<Human> temp = new ArrayList<>();
                temp.add(h);
                result.get(age).put(fChar, temp);
            } else {
                result.get(age).get(fChar).add(h);
            }
        }
        for (Integer i : result.keySet()) {
            for (Character ch : result.get(i).keySet()) {
                result.get(i).get(ch).sort(Comparator.comparing(Human::getLastName)
                        .thenComparing(Human::getFirstName)
                        .thenComparing(Human::getMiddleName));
                Collections.reverse(result.get(i).get(ch));
            }
        }
        return result;
    }
}
