package сlasses;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class Human implements Comparable<Human> {
    private String firstName = "Sample Text",
            middleName = "Sample Text",
            lastName = "Sample Text";
    private int age = 0;

    public Human(int age) {
        setAge(age);
    }
    public Human() {
    }

    public Human(int age, StringBuilder fName, StringBuilder mName, StringBuilder lName) {
        setFirstName(fName.toString());
        setMiddleName(mName.toString());
        setLastName(lName.toString());
        setAge(age);
    }

    public Human(int age, String fName, String mName, String lName) {
        setFirstName(fName);
        setMiddleName(mName);
        setLastName(lName);
        setAge(age);
    }
    public Human( String fName, String mName, String lName) {
        setFirstName(fName);
        setMiddleName(mName);
        setLastName(lName);
        setAge(0);
    }

    public Human iClone() {
        return new Human(this);
    }

    public Human(@NotNull Human copy) {
        setFirstName(copy.firstName);
        setMiddleName(copy.middleName);
        setLastName(copy.lastName);
        setAge(copy.age);
    }

    public void setFirstName(String name) {
        this.firstName = setName(name, "first");
    }

    public void setMiddleName(String name) {
        this.middleName = setName(name, "middle");
    }

    public void setLastName(String name) {
        this.lastName = setName(name, "last");
    }

    protected String setName(@NotNull String name, String string) {
        if (name.length() == 0) {
            throw new IllegalArgumentException("Empty " + string + " name");
        }
        return name;
    }

    public void setAge(int age) {
        if (age < 0) {
            throw new IllegalArgumentException("Age must be non negative");
        }
        this.age = age;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public int getAge() {
        return age;
    }


    @Override
    public int compareTo(@NotNull Human o) {
        int result = this.getLastName().compareTo(o.getLastName());
        if (result != 0) return result;

        result = this.getFirstName().compareTo(o.getFirstName());
        if (result != 0) return result;

        result = this.getMiddleName().compareTo(o.getMiddleName());
        if (result != 0) return result;

        result = getAge() - o.getAge();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return age == human.age &&
                firstName.compareTo(human.firstName) == 0 &&
                middleName.compareTo(human.middleName) == 0 &&
                lastName.compareTo(human.lastName) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, middleName, lastName, age);
    }
}