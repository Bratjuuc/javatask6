package сlasses;

import java.util.Objects;

public class Tuple<T, M> {
    private T fst;
    private M snd;

    public Tuple(T fst, M snd) {
        this.fst = fst;
        this.snd = snd;
    }

    public T getFst() {
        return fst;
    }

    public M getSnd() {
        return snd;
    }

    public void setFst(T fst) {
        this.fst = fst;
    }

    public void setSnd(M snd) {
        this.snd = snd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tuple)) return false;
        Tuple<?, ?> tuple = (Tuple<?, ?>) o;
        return fst.equals(tuple.fst) &&
                snd.equals(tuple.snd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fst, snd);
    }
}