package сlasses;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.function.Function;

public class FuncProg {
    public static <T, M extends Collection<T>> M filter(@NotNull M lst, Function<T, Boolean> predicate, M result) {
        for (T i : lst) {
            if (predicate.apply(i)) {
                result.add(i);
            }
        }
        return result;
    }

    public static <T1, M1 extends Collection<T1>, T2, M2 extends Collection<T2>> M2 map(Function<T1, T2> func, @NotNull M1 collection, M2 result) {
        for (T1 i : collection) {
            result.add(func.apply(i));
        }
        return result;
    }

    /*из пушки по воробьям
    public static <T, M extends Collection<T>> Integer count(M lst, Function<T, Boolean> predicate){
        Integer result = 0;
        for (T i : lst){
            if (predicate.apply(i)){
                result++;
            }
        }
        return result;
    }*/
}