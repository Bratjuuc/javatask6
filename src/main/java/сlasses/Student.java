package сlasses;

public class Student extends Human {
    public String department = "Sample Text";

    public Student(Human human, String department) {
        super(human);
        setDepartment(department);
    }

    public Student(int age, String fName, String mName, String lName, String department) {
        super(age, fName, mName, lName);
        setDepartment(department);
    }
    public Student( String fName, String mName, String lName, String department) {
        super(fName, mName, lName);
        setDepartment(department);
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = setName(department, "department");
    }

    @Override
    public Student iClone() {
        return new Student(this, department);
    }

    @Override
    public boolean equals(Object o) {
        Student student = (Student) o;
        return super.equals(o) && department.compareTo(student.department) == 0;
    }
}